# Großübung Statistik 

## Tutorial

Dieses Tutorial ist als Teil der Bachelorveranstaltung Statistik der Wirtschaftswissenschaftlichen Fakultät an der Universität Göttingen entstanden und 
soll Studierenden helfen die Inhalte der Vorlesung zu üben. Das Skript umfasst Videos und einfache Quizfragen, die hoffentlich motivieren zum Selberrechnen und zur Vorbereitung der Klausur.

Um das Tutorial zu starten, einfach auf folgenden Button klicken
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Fberner7%2Fstatistik.git/master?urlpath=rstudio)

Wenn das Programm das erste mal gestartet wird, werden alle nötigen Pakete in R gestartet. Dieser Vorgang kann einige Minuten dauern. Also am Besten erst mal einen Kaffee machen. Stellen Sie auch sicher, dass der Browser Pop-Up Fenster zulässt.


## Verweise
Das Skript bezieht sich auf verschiedene Quellen, deren inhaltliche Aufarbeitung und Darstellung der Thematik, als Grundlage und Bezugspunkt beim Erstellen dieses Skriptes dienten. Hierbei sei vor allem auf folgende Quellen verwiesen, wobei es sich bei allen Quellen um Free-and-Open-Source Lehrinhalte der jeweiligen Autoren handelt:
* https://github.com/oneilsh/learnr-demo/blob/master/README.md